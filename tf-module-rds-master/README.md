
# Module `tf-module-rds`

Core Version Constraints:
* `~> 0.12.0`

Provider Requirements:
* **aws:** `~> 2.54.0`
* **null:** `~> 2.1.2`
* **random:** `~> 2.0`

## Input Variables
* `account` (required): Account, e.g 'cis', 'cit', 'cdr' ...
* `admin_user` (default `"adminuser"`): Username for the master DB user
* `allocated_storage` (required): The allocated storage in gigabytes. If creating DB instance from a snapshot, you may set this to null
* `allowed_cidr_blocks` (default `[]`): List of CIDR blocks allowed to access the cluster on SSL port
* `allowed_cidr_blocks_unsecure` (default `[]`): List of CIDR blocks allowed to access the cluster on non-SSL port
* `apply_immediately` (default `false`): Specifies whether any database modifications are applied immediately, or during the next maintenance window
* `auto_minor_version_upgrade` (default `false`): Indicates that minor engine upgrades will be applied automatically to the DB instance during the maintenance window
* `availability_zone` (default `null`): The AZ for the RDS instance
* `backup_retention_period` (default `"prod"`): Backup retention. Valid values are 'prod', 'non-prod' or any number of days. BV standard is 31 for prod and 15 for non-prod.
* `backup_window` (default `"00:00-00:30"`): Daily time range during which the backups happen. Default value is BV standard.
* `bv_dns_aliases` (default `[]`): Optional list of dns aliases for documentation purpose only
* `character_set_name` (default `null`): The character set name to use for DB encoding in Oracle instances. This can't be changed. See https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Appendix.OracleCharacterSets.html for more information
* `cpm_dr_encryption_key_source_region` (default `"arn:aws:kms:eu-west-1:279898079618:key/c2290c18-11e6-4742-8b42-902c9ff63f08"`): KMS key for DR on Backup account in same region than source
* `cpm_dr_encryption_key_target_region` (default `"arn:aws:kms:eu-west-3:279898079618:key/1d9fdfb2-6298-42ff-8386-33e49a7ea39e"`): KMS key for DR on Backup account in target region
* `custom_billing` (default `[]`): A list of ([gap_code],[environment]) for custom billing purpose, i.e. where billing is not the same than naming (mainly for shared resources).
* `db_name` (default `null`): The name of the database to create when the DB instance is created. If this parameter is not specified, no database is created in the DB instance. Note that this does not apply for Oracle or SQL Server engines. See the [documentation](https://docs.aws.amazon.com/cli/latest/reference/rds/create-db-instance.html#options) for more details on what applies for those engines.
* `db_port` (default `null`): Database port. Defaults to the default port for the selected database type
* `deletion_protection` (default `true`): If the DB instance should have deletion protection enabled
* `enable_backup_dr` (default `false`): Enable replication of backup for DR
* `enabled_cloudwatch_logs_exports` (default `[]`): List of log types to export to cloudwatch. If omitted, no logs will be exported. Valid values (depending on engine): alert, audit, error, general, listener, slowquery, trace, postgresql (PostgreSQL), upgrade (PostgreSQL)
* `enabled_cpm_backup` (default `"Auto"`): Allow to enable CPM backup. One of Auto, true or false. Auto will choose using environment: true for pr, and false for others.
* `enabled_enhanced_monitoring` (default `false`): Set to false to prevent the module creating enhanced monitoring configuration and resources
* `engine` (required): The database engine to use ('mysql', 'mariadb', 'postgresql', 'oracle-ee', 'sqlserver-se', ...)
* `engine_version` (required): The version number of the database engine to use
* `environment` (required): Environment: 'dv', 'in', 'ut', 'pp', 'pr'
* `gap_code` (required): BV Project code
* `instance_options` (default `[]`): List of DB instance options to apply. See https://www.terraform.io/docs/providers/aws/r/db_option_group.html for available attributes.
* `instance_parameters` (default `[]`): List of DB instance parameters to apply. Each parameter should specify a name and value, and an optional apply_method
* `instance_type` (default `"db.t3.small"`): Instance type to use
* `kms_key_arn` (required): KMS Key ARN to use for encryption
* `license_model` (default `null`): License model information for this DB instance (required for some DB engines, eg: Oracle SE1). Valid values: license-included, bring-your-own-license, general-public-license
* `maintenance_window` (default `"Sun:04:00-Sun:04:30"`): Weekly time range during which system maintenance can occur, in UTC. Default value is BV standard.
* `map-migrated` (default `null`): Unique Id provided by AWS migration Hub for the AWS MAP programm
* `max_allocated_storage` (default `0`): Specifies the value for Storage Autoscaling. Must be greater than or equal to `allocated_storage` or 0 to disable Storage Autoscaling
* `monitoring_interval` (default `0`): Interval in seconds that metrics are collected, 0 to disable (values can only be 0, 1, 5, 10, 15, 30, 60)
* `multi_az` (default `false`): If the RDS instance is multi AZ enabled.
* `number` (required): Incremental number, e.g. '1, '2', '3' ...
* `performance_insights_enabled` (default `true`): Whether to enable Performance Insights
* `performance_insights_kms_key_id` (default `""`): The ARN for the KMS key to encrypt Performance Insights data. When specifying `performance_insights_kms_key_id`, `performance_insights_enabled` needs to be set to true. Once KMS key is set, it can never be changed
* `performance_insights_retention_period` (default `7`): The amount of time in days to retain Performance Insights data. Either 7 (7 days) or 731 (2 years). When specifying `performance_insights_retention_period`, `performance_insights_enabled` needs to be set to true
* `profile` (required): AWS Profile used to deploy your resources
* `region` (required): AWS region used to deploy your resources
* `schedule_start` (default `[]`): Cron expression used to automatically start RDS instance. Possible to define multiple cron expression separate by '//' (double-slash)
* `schedule_stop` (default `[]`): Cron expression used to automatically start RDS instance. Possible to define multiple cron expression separate by '//' (double-slash)
* `skip_final_snapshot` (default `false`): Determines whether a final DB snapshot is created before the DB cluster is deleted
* `snapshot_identifier` (default `null`): Specifies whether or not to create this database from a snapshot. This correlates to the snapshot ID you'd find in the RDS console, eg: rds:production-2015-06-26-06-05.
* `storage_iops` (default `null`): The amount of provisioned IOPS. Setting this implies a storage_type of io1
* `storage_type` (default `null`): One of standard, gp2 or io1. The default is io1 if iops is specified, standard if not
* `subnet_ids` (required): Subnet identifiers for DB side
* `vpc_id` (required): VPC identifier

## Output Values
* `address`: The address of the RDS instance
* `arn`: The ARN of the RDS instance
* `availability_zone`: The availability zone of the RDS instance
* `id`: The RDS instance ID
* `password`: Auto generated password for the main DB user. IMPORTANT: this should be changed after provisioning
* `port`: The database port
* `security_group_id`: SG for additionnal rules added by client components: application server, batches
* `ssl_port`: The database SSL port
* `status`: The RDS instance status

## Managed Resources
* `aws_db_instance.rds` from `aws`
* `aws_db_instance_role_association.s3_integration` from `aws`
* `aws_db_option_group.rds` from `aws`
* `aws_db_parameter_group.rds` from `aws`
* `aws_db_subnet_group.rds` from `aws`
* `aws_iam_role.enhanced_monitoring` from `aws`
* `aws_iam_role.s3_integration` from `aws`
* `aws_iam_role_policy.allow_s3_and_kms` from `aws`
* `aws_iam_role_policy_attachment.enhanced_monitoring` from `aws`
* `aws_security_group.rds` from `aws`
* `aws_security_group_rule.rds` from `aws`
* `aws_security_group_rule.rds_unsecure` from `aws`
* `null_resource.update_db_password` from `null`
* `random_password.rds_password` from `random`

## Data Resources
* `data.aws_arn.cpm_dr_encryption_key_source_region` from `aws`
* `data.aws_arn.cpm_dr_encryption_key_target_region` from `aws`
* `data.aws_iam_policy_document.allow_s3_and_kms` from `aws`
* `data.aws_iam_policy_document.assume_rds_role` from `aws`
* `data.aws_iam_policy_document.enhanced_monitoring` from `aws`

## Child Modules
* `naming-rds` from `git::https://git.bureauveritas.com/aws-cloud/security/tf-module-bv-naming.git//?ref=release-1.2.24`
* `naming-rds-groups` from `git::https://git.bureauveritas.com/aws-cloud/security/tf-module-bv-naming.git//?ref=release-1.2.24`
* `naming-sg` from `git::https://git.bureauveritas.com/aws-cloud/security/tf-module-bv-naming.git//?ref=release-1.2.24`

