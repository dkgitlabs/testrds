data "aws_arn" "cpm_dr_encryption_key_source_region" {
  arn = var.cpm_dr_encryption_key_source_region
}

data "aws_arn" "cpm_dr_encryption_key_target_region" {
  arn = var.cpm_dr_encryption_key_target_region
}