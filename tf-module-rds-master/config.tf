terraform {
  backend "s3" {
    bucket = "dklabsterraformstate"
    key    = "terraformtrn.tfstate"
    region = "eu-west-1"
    profile = "TRN"
    }
  #required_version = "~> 0.12.0"  //Commented by DK
  required_version = ">= 0.12.0"

  required_providers {
    aws    = "~> 2.54.0"
    random = "~> 2.0"
    null   = "~> 2.1.2"
  }
}
