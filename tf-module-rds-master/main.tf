# Naming convention module for RDS
module "naming-rds" {
  #source = "git::https://git.bureauveritas.com/aws-cloud/security/tf-module-bv-naming.git//?ref=release-1.2.24"
  #source = "git::http://gitlab.com/dkgitlabs/testrds/-/tree/master/tf-module-bv-naming-master"
  source         = "../tf-module-bv-naming-master"
  account        = var.account
  gap_code       = var.gap_code
  environment    = var.environment
  custom_billing = var.custom_billing
  specification  = lookup(local.engine_dbtype_map, var.engine)
  number         = var.number
}

module "naming-rds-groups" {
  #source = "git::https://git.bureauveritas.com/aws-cloud/security/tf-module-bv-naming.git//?ref=release-1.2.24"
  #source = "git::http://gitlab.com/dkgitlabs/testrds/-/tree/master/tf-module-bv-naming-master"
  source         = "../tf-module-bv-naming-master"
  account       = var.account
  gap_code      = var.gap_code
  environment   = var.environment
  specification = "${lookup(local.engine_dbengine_map, var.engine)}-${join("", regex("(\\d+)\\..*", var.engine_version))}"
  number        = var.number
}

# Naming convention module for Security group
module "naming-sg" {
  #source = "git::https://git.bureauveritas.com/aws-cloud/security/tf-module-bv-naming.git//?ref=release-1.2.24"
  #source = "git::http://gitlab.com/dkgitlabs/testrds/-/tree/master/tf-module-bv-naming-master"
  source         = "../tf-module-bv-naming-master"
  account        = var.account
  gap_code       = var.gap_code
  environment    = var.environment
  custom_billing = var.custom_billing
  specification  = "db-${lookup(local.engine_dbtype_map, var.engine)}-${var.number}"
}

# Local variables
locals {
  # Map RDS engines to dbtype trigrams
  engine_dbtype_map = {
    "postgres"      = "pgs"
    "mysql"         = "mys"
    "mariadb"       = "mar"
    "sqlserver-ee"  = "sql"
    "sqlserver-se"  = "sql"
    "sqlserver-ex"  = "sql"
    "sqlserver-web" = "sql"
    "oracle-ee"     = "ora"
    "oracle-se1"    = "ora"
    "oracle-se2"    = "ora"
    "oracle-se"     = "ora"
  }
  # Map RDS engines to dbengine names
  engine_dbengine_map = {
    "postgres"      = "pgsql"
    "mysql"         = "mysql"
    "mariadb"       = "maria"
    "sqlserver-ee"  = "sqlee"
    "sqlserver-se"  = "sqlse"
    "sqlserver-ex"  = "sqlex"
    "sqlserver-web" = "sqlweb"
    "oracle-ee"     = "oraee"
    "oracle-se1"    = "orase1"
    "oracle-se2"    = "orase2"
    "oracle-se"     = "orase"
  }

  # Build the parameter group family name
  pg_family_standard_formats = {
    "v_major" = format("%s%s", var.engine, regex("(\\d+).*", var.engine_version)[0])
    "v_minor" = format("%s%s", var.engine, regex("(\\d+\\.\\d+).*", var.engine_version)[0])
  }
  pg_family_hyphenated_formats = {
    "v_major" = format("%s-%s", var.engine, regex("(\\d+).*", var.engine_version)[0])
    "v_minor" = format("%s-%s", var.engine, regex("(\\d+\\.\\d+).*", var.engine_version)[0])
  }
  pg_families_map = {
    "postgres" = {
      # eg. postgres9.6
      "9" = local.pg_family_standard_formats.v_minor
      # eg. postgres10 / postgres11
      "default" = local.pg_family_standard_formats.v_major
    }
    "mysql" = {
      # eg. mysql5.6
      "default" = local.pg_family_standard_formats.v_minor
    }
    "mariadb" = {
      # eg. mariadb10.1
      "default" = local.pg_family_standard_formats.v_minor
    }
    "sqlserver-ee" = {
      # eg. sqlserver-ee-14.0
      "default" = substr(local.pg_family_hyphenated_formats.v_minor, 0, length(local.pg_family_hyphenated_formats.v_minor) - 1)
    }
    "sqlserver-se" = {
      # eg. sqlserver-se-14.0
      "default" = substr(local.pg_family_hyphenated_formats.v_minor, 0, length(local.pg_family_hyphenated_formats.v_minor) - 1)
    }
    "sqlserver-ex" = {
      # eg. sqlserver-ex-14.0
      "default" = substr(local.pg_family_hyphenated_formats.v_minor, 0, length(local.pg_family_hyphenated_formats.v_minor) - 1)
    }
    "sqlserver-web" = {
      # eg. sqlserver-web-14.0
      "default" = substr(local.pg_family_hyphenated_formats.v_minor, 0, length(local.pg_family_hyphenated_formats.v_minor) - 1)
    }
    "oracle-ee" = {
      # eg. oracle-ee-12.2
      "default" = local.pg_family_hyphenated_formats.v_minor
    }
    "oracle-se1" = {
      # eg. oracle-se1-11.2
      "default" = local.pg_family_hyphenated_formats.v_minor
    }
    "oracle-se2" = {
      # eg. oracle-se2-12.1
      "default" = local.pg_family_hyphenated_formats.v_minor
    }
    "oracle-se" = {
      # eg. oracle-se-11.2
      "default" = local.pg_family_hyphenated_formats.v_minor
    }
  }
  pg_family = lookup(lookup(local.pg_families_map, var.engine), split(".", var.engine_version)[0], lookup(lookup(local.pg_families_map, var.engine), "default"))

  # Prepare extra tags to add to resource
  schedule_start_expression = replace(replace(replace(join(" // ", var.schedule_start), ",", "."), "?", "@"), "*", "+")
  schedule_stop_expression  = replace(replace(replace(join(" // ", var.schedule_stop), ",", "."), "?", "@"), "*", "+")
  schedule_start_tag        = local.schedule_start_expression != "" ? { "bv:schedule_start" : local.schedule_start_expression } : {}
  schedule_stop_tag         = local.schedule_stop_expression != "" ? { "bv:schedule_stop" : local.schedule_stop_expression } : {}
  dns_aliases_tag           = length(var.bv_dns_aliases) > 0 ? { "bv:dns_aliases" = join("+", var.bv_dns_aliases) } : {}
  rds_extra_tags            = merge(
    var.map-migrated == null ? {} : { map-migrated = var.map-migrated },
    local.dns_aliases_tag,
    local.schedule_start_tag,
    local.schedule_stop_tag,
    // CPM DR feature
    var.enable_backup_dr ? {
      "cpm backup" = "${upper(var.account)}_RDS_DR_Policy",
      "cpm_dr_encryption_key:${data.aws_arn.cpm_dr_encryption_key_source_region.region}" = var.cpm_dr_encryption_key_source_region,
      "cpm_dr_encryption_key:${data.aws_arn.cpm_dr_encryption_key_target_region.region}" = var.cpm_dr_encryption_key_target_region
    } :
    { "cpm backup" = "no-backup" }
  )

  create_option_group = var.engine != "postgres"

  # Compute retention period. Valid values are 'prod', 'non-prod' or any number of days. BV standard is 31 for prod and 15 for non-prod."
  backup_retention_period = (var.backup_retention_period == "prod") ? 31 : (var.backup_retention_period == "non-prod" ? 15 : var.backup_retention_period)

  force_ssl_parameter = {
    "postgres"      = { "name" = "rds.force_ssl", "value" = 1, "apply_method" = "immediate" }
    "sqlserver-ee"  = { "name" = "rds.force_ssl", "value" = 1, "apply_method" = "pending-reboot" }
    "sqlserver-se"  = { "name" = "rds.force_ssl", "value" = 1, "apply_method" = "pending-reboot" }
    "sqlserver-ex"  = { "name" = "rds.force_ssl", "value" = 1, "apply_method" = "pending-reboot" }
    "sqlserver-web" = { "name" = "rds.force_ssl", "value" = 1, "apply_method" = "pending-reboot" }
  }
  instance_parameters = [
    for v in distinct(flatten([
      [for p in var.instance_parameters : p if p.name != "rds.force_ssl"],
      [lookup(local.force_ssl_parameter, var.engine, null)]
    ])) : v if v != null
  ]

  is_sqlserver = lookup(local.engine_dbtype_map, var.engine) != "sql" ? false : true
  is_oracle    = lookup(local.engine_dbtype_map, var.engine) != "ora" ? false : true

  ssl_port = local.is_oracle ? 2484 : -1

  # Include automatically some instance options
  instance_options = [
    for v in flatten([
      [for o in var.instance_options : o if o.option_name != "SSL"],

      # ----------------------------------------------------------------------------
      # For Oracle:
      # ----------------------------------------------------------------------------
      [
        # Force SSL - See https://docs.aws.amazon.com/fr_fr/AmazonRDS/latest/UserGuide/Appendix.Oracle.Options.SSL.html
        ! local.is_oracle ? null :
        {
          option_name                    = "SSL"
          port                           = local.ssl_port
          vpc_security_group_memberships = [aws_security_group.rds.id]

          option_settings = [
            {
              name  = "SQLNET.SSL_VERSION",
              value = "1.2"
            }
          ]
        }
      ],
      [
        # S3 integration - See https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/oracle-s3-integration.html
        ! local.is_oracle ? null :
        {
          option_name = "S3_INTEGRATION"
          version     = "1.0"
        }
      ],

      # ----------------------------------------------------------------------------
      # For SQL Server:
      # ----------------------------------------------------------------------------
      [
        # S3 integration - See https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Appendix.SQLServer.Options.BackupRestore.html
        ! local.is_sqlserver ? null :
        {
          option_name = "SQLSERVER_BACKUP_RESTORE"

          option_settings = [
            {
              name  = "IAM_ROLE_ARN",
              value = aws_iam_role.s3_integration[0].arn
            }
          ]
        }
      ]

    ]) : v if v != null
  ]
}

# ----------------------------------------------------------------------------
# Enhanced Monitoring
# ----------------------------------------------------------------------------

# allow rds to assume this role
data "aws_iam_policy_document" "enhanced_monitoring" {
  statement {
    effect  = "Allow"
    actions = [ "sts:AssumeRole" ]

    principals {
      type        = "Service"
      identifiers = [ "monitoring.rds.amazonaws.com" ]
    }
  }
}

# create IAM role for monitoring
resource "aws_iam_role" "enhanced_monitoring" {
  count              = var.enabled_enhanced_monitoring ? 1 : 0
  name               = "${var.account}-rds-enhanced_monitoring_for_${module.naming-rds.bv_rds_name}"
  description        = "RDS enhanced monitoring role for: ${module.naming-rds.bv_rds_name}"
  assume_role_policy = data.aws_iam_policy_document.enhanced_monitoring.json
}

# Attach Amazon's managed policy for RDS enhanced monitoring
resource "aws_iam_role_policy_attachment" "enhanced_monitoring" {
  count      = var.enabled_enhanced_monitoring ? 1 : 0
  role       = aws_iam_role.enhanced_monitoring[0].name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

# ----------------------------------------------------------------------------
# Security Group
# ----------------------------------------------------------------------------

# SG for default rules server side
resource "aws_security_group" "rds" {
  name        = module.naming-sg.bv_security_group_name
  description = "SG for RDS instance: ${module.naming-rds.bv_rds_name}"
  vpc_id      = var.vpc_id

  tags = merge(
    module.naming-sg.bv_standard_tags,
    { Name = module.naming-sg.bv_security_group_name }
  )

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [ "0.0.0.0/0" ]
    description = "Allow all"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Manage SG rules in separate resource to allow external modules to add their own rules
# Access to secure SSL port
resource "aws_security_group_rule" "rds" {
  for_each = toset(var.allowed_cidr_blocks)

  type        = "ingress"
  from_port   = local.is_oracle ? 2484 : aws_db_instance.rds.port
  to_port     = local.is_oracle ? 2484 : aws_db_instance.rds.port
  protocol    = "tcp"
  cidr_blocks = [ each.value ]
  description = "Custom allowed access on SSL port"

  security_group_id = aws_security_group.rds.id
}
/* //by DK
# Access to unsecure non-SSL port
resource "aws_security_group_rule" "rds_unsecure" {
  for_each    = toset(var.allowed_cidr_blocks_unsecure)

  type        = "ingress"
  from_port   = local.is_oracle ? 1521 : aws_db_instance.rds.port
  to_port     = local.is_oracle ? 1521 : aws_db_instance.rds.port
  protocol    = "tcp"
  cidr_blocks = [ each.value ]
  description = "Custom allowed access on non-SSL port"

  security_group_id = aws_security_group.rds.id
}
*/
# ----------------------------------------------------------------------------
# Instance
# ----------------------------------------------------------------------------

resource "aws_db_subnet_group" "rds" {
  name        = module.naming-rds.bv_rds_name
  description = "Allowed subnets for RDS instance: ${module.naming-rds.bv_rds_name}"
  subnet_ids  = var.subnet_ids

  tags = module.naming-rds.bv_standard_tags
}

resource "aws_db_parameter_group" "rds" {
  name        = module.naming-rds-groups.bv_rds_parameter_group_name
  description = "Parameter group for instance: ${module.naming-rds.bv_rds_name}"
  family      = local.pg_family

  tags = module.naming-rds.bv_standard_tags

  dynamic "parameter" {
    for_each = local.instance_parameters
    content {
      name         = parameter.value.name
      value        = parameter.value.value
      apply_method = lookup(parameter.value, "apply_method", "pending-reboot")
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_db_option_group" "rds" {
  count = local.create_option_group ? 1 : 0

  name                     = module.naming-rds-groups.bv_rds_option_group_name
  option_group_description = "Option group for instance: ${module.naming-rds.bv_rds_name}"
  engine_name              = var.engine
  major_engine_version     = regex("(\\d+\\.\\d+).*", var.engine_version)[0]

  tags = module.naming-rds.bv_standard_tags

  dynamic "option" {
    for_each = local.instance_options
    content {
      option_name                    = option.value.option_name
      port                           = lookup(option.value, "port", null)
      version                        = lookup(option.value, "version", null)
      db_security_group_memberships  = lookup(option.value, "db_security_group_memberships", null)
      vpc_security_group_memberships = lookup(option.value, "vpc_security_group_memberships", null)

      dynamic "option_settings" {
        for_each = lookup(option.value, "option_settings", [])
        content {
          name  = lookup(option_settings.value, "name", null)
          value = lookup(option_settings.value, "value", null)
        }
      }
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_db_instance" "rds" {
  identifier = module.naming-rds.bv_rds_name
  tags = merge(module.naming-rds.bv_standard_tags, local.rds_extra_tags)

  engine         = var.engine
  engine_version = var.engine_version
  instance_class = var.instance_type
  license_model  = var.license_model
  multi_az       = var.multi_az

  allocated_storage     = var.allocated_storage
  max_allocated_storage = var.max_allocated_storage
  storage_type          = var.storage_type
  iops                  = var.storage_iops
  storage_encrypted     = true
  kms_key_id            = var.kms_key_arn

  name     = var.db_name
  username = var.admin_user
  password = random_password.rds_password.result
  port     = var.db_port

  snapshot_identifier = var.snapshot_identifier

  vpc_security_group_ids = [aws_security_group.rds.id]
  db_subnet_group_name   = aws_db_subnet_group.rds.id
  availability_zone      = var.availability_zone
  publicly_accessible    = false

  parameter_group_name = aws_db_parameter_group.rds.id
  option_group_name    = local.create_option_group ? aws_db_option_group.rds[0].id : null

  monitoring_interval = var.monitoring_interval
  monitoring_role_arn = var.enabled_enhanced_monitoring ? aws_iam_role.enhanced_monitoring[0].arn : null

  allow_major_version_upgrade = true
  auto_minor_version_upgrade  = var.auto_minor_version_upgrade
  apply_immediately           = var.apply_immediately
  maintenance_window          = var.maintenance_window
  skip_final_snapshot         = var.skip_final_snapshot
  copy_tags_to_snapshot       = true
  final_snapshot_identifier   = lower(module.naming-rds.bv_rds_name)

  performance_insights_enabled          = var.performance_insights_enabled
  performance_insights_retention_period = var.performance_insights_enabled ? var.performance_insights_retention_period : null
  performance_insights_kms_key_id       = var.performance_insights_kms_key_id

  backup_retention_period = local.backup_retention_period
  backup_window           = var.backup_window

  character_set_name = var.character_set_name

  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports

  deletion_protection = var.deletion_protection

  lifecycle {
    ignore_changes = [password]
  }
}

# Set a default random password for creation.
# It will be updated after creation by the "update_db_password" routine using aws cli to make sure password is not stored in Terraform state
resource "random_password" "rds_password" {
  length           = 16
  special          = true
  override_special = "!#-_=+"
}

# Update database password using aws cli to make sure password is not stored in Terraform state
resource "null_resource" "update_db_password" {
  triggers = {
    db_arn = aws_db_instance.rds.arn
  }

  provisioner "local-exec" {
    command = "aws rds modify-db-instance --db-instance-identifier ${aws_db_instance.rds.id} --master-user-password ${substr(uuid(), 0, 29)} --profile ${var.profile} --region ${var.region}"
  }
}

# Oracle S3 integration - See https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/oracle-s3-integration.html
resource "aws_db_instance_role_association" "s3_integration" {
  count                  = local.is_oracle ? 1 : 0
  db_instance_identifier = aws_db_instance.rds.id
  feature_name           = "S3_INTEGRATION"
  role_arn               = aws_iam_role.s3_integration[0].arn
}

# S3 integration RDS role
resource "aws_iam_role" "s3_integration" {
  count              = local.is_oracle || local.is_sqlserver ? 1 : 0
  name               = "${var.account}-rds-s3_integration-${module.naming-rds.bv_rds_name}"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.assume_rds_role[0].json
}

# Allow Assume RDS service role
data "aws_iam_policy_document" "assume_rds_role" {
  count = local.is_oracle || local.is_sqlserver ? 1 : 0

  statement {
    effect  = "Allow"
    actions = [ "sts:AssumeRole" ]

    principals {
      type        = "Service"
      identifiers = [ "rds.amazonaws.com" ]
    }
  }
}

# Allow S3 access
resource "aws_iam_role_policy" "allow_s3_and_kms" {
  count  = local.is_oracle || local.is_sqlserver ? 1 : 0
  name   = "allow_s3_backup_bucket_access_and_kms"
  role   = aws_iam_role.s3_integration[0].name
  policy = data.aws_iam_policy_document.allow_s3_and_kms[0].json
}

# Allow S3 access
data "aws_iam_policy_document" "allow_s3_and_kms" {
  count = local.is_oracle || local.is_sqlserver ? 1 : 0
  statement {
    sid    = "AllowS3BackupBucket"
    effect = "Allow"
    actions = [
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObject",
      "s3:AbortMultipartUpload",
      "s3:ListMultipartUploadParts"
    ]
    resources = [
      "arn:aws:s3:::${var.account}-awsrds-backups",
      "arn:aws:s3:::${var.account}-awsrds-backups/*"
    ]
  }

  statement {
    sid    = "AllowKMS"
    effect = "Allow"
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:GenerateDataKey",
      "kms:DescribeKey"
    ]
    resources = [ "*" ]
  }
}
