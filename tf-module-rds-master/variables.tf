variable "region" {
  type        = string
  default     = "eu-west-1" // Added by DK
  description = "AWS region used to deploy your resources"
}

variable "profile" {
  type        = string
  default     = "TRN"
  description = "AWS Profile used to deploy your resources"
}

variable "account" {
  type        = string
  default     = "trn"   //Added by DK
  description = "Account, e.g 'cis', 'cit', 'cdr' ,'trn',..."
}

variable "gap_code" {
  type        = string
  description = "BV Project code"
}

variable "environment" {
  type        = string
  description = "Environment: 'dv', 'in', 'ut', 'pp', 'pr'"
}

variable "custom_billing" {
  type = list(object({
    gap_code    = string
    environment = string
  }))
  default     = []
  description = "A list of ([gap_code],[environment]) for custom billing purpose, i.e. where billing is not the same than naming (mainly for shared resources)."
}

variable "map-migrated" {
  description = "Unique Id provided by AWS migration Hub for the AWS MAP programm"
  type        = string
  default     = null
}

variable "number" {
  type        = string
  description = "Incremental number, e.g. '1, '2', '3' ..."
}

variable "kms_key_arn" {
  type        = string
  description = "KMS Key ARN to use for encryption"
  default     = "arn:aws:kms:eu-west-1:682426607174:key/9054bb51-86a4-441a-96d3-2a59a7bc8b88" //Added by DK
}

variable "bv_dns_aliases" {
  type        = list(string)
  default     = []
  description = "Optional list of dns aliases for documentation purpose only"
}

variable "vpc_id" {
  type        = string
  default     =  "vpc-00be10e2ec6669a26"  // Added by DK
  description = "VPC identifier"
}

variable "subnet_ids" {
  type        = list(string)
  default     = ["subnet-05180ab3dee0913e0", "subnet-004a4357a50f4ab77"] //Added by DK
  description = "Subnet identifiers for DB side"
}

variable "availability_zone" {
  type        = string
  default     = null
  description = "The AZ for the RDS instance"
}

variable "instance_type" {
  type        = string
  default     = "db.t3.small"
  description = "Instance type to use"
}

variable "engine" {
  type        = string
  description = "The database engine to use ('mysql', 'mariadb', 'postgresql', 'oracle-ee', 'sqlserver-se', ...)"
}

variable "engine_version" {
  type        = string
  description = "The version number of the database engine to use"
}

variable "db_name" {
  type        = string
  default     = null
  description = "The name of the database to create when the DB instance is created. If this parameter is not specified, no database is created in the DB instance. Note that this does not apply for Oracle or SQL Server engines. See the [documentation](https://docs.aws.amazon.com/cli/latest/reference/rds/create-db-instance.html#options) for more details on what applies for those engines."
}

variable "db_port" {
  type        = number
  default     = null
  description = "Database port. Defaults to the default port for the selected database type"
}

variable "admin_user" {
  type        = string
  default     = "adminuser"
  description = "Username for the master DB user"
}

variable "allocated_storage" {
  type        = number
  description = "The allocated storage in gigabytes. If creating DB instance from a snapshot, you may set this to null"
}

variable "max_allocated_storage" {
  description = "Specifies the value for Storage Autoscaling. Must be greater than or equal to `allocated_storage` or 0 to disable Storage Autoscaling"
  type        = number
  default     = 0
}

variable "storage_type" {
  type        = string
  default     = null
  description = "One of standard, gp2 or io1. The default is io1 if iops is specified, standard if not"
}

variable "storage_iops" {
  type        = number
  default     = null
  description = "The amount of provisioned IOPS. Setting this implies a storage_type of io1"
}

variable "snapshot_identifier" {
  type        = number
  default     = null
  description = "Specifies whether or not to create this database from a snapshot. This correlates to the snapshot ID you'd find in the RDS console, eg: rds:production-2015-06-26-06-05."
}

variable "license_model" {
  type        = string
  default     = null
  description = "License model information for this DB instance (required for some DB engines, eg: Oracle SE1). Valid values: license-included, bring-your-own-license, general-public-license"
}

variable "multi_az" {
  type        = bool
  default     = false
  description = "If the RDS instance is multi AZ enabled."
}

variable "instance_parameters" {
  type        = list(map(string))
  default     = []
  description = "List of DB instance parameters to apply. Each parameter should specify a name and value, and an optional apply_method"
}

variable "instance_options" {
  type        = list(map(any))
  default     = []
  description = "List of DB instance options to apply. See https://www.terraform.io/docs/providers/aws/r/db_option_group.html for available attributes."
}

variable "character_set_name" {
  description = "The character set name to use for DB encoding in Oracle instances. This can't be changed. See https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Appendix.OracleCharacterSets.html for more information"
  type        = string
  default     = null
}

variable "allowed_cidr_blocks" {
  type        = list(string)
  default     = []
  description = "List of CIDR blocks allowed to access the cluster on SSL port"
}

variable "allowed_cidr_blocks_unsecure" {
  type        = list(string)
  default     = []
  description = "List of CIDR blocks allowed to access the cluster on non-SSL port"
}

variable "skip_final_snapshot" {
  type        = bool
  description = "Determines whether a final DB snapshot is created before the DB cluster is deleted"
  default     = true  //Changed false to true by DK
}

variable "maintenance_window" {
  type        = string
  default     = "Sun:04:00-Sun:04:30"
  description = "Weekly time range during which system maintenance can occur, in UTC. Default value is BV standard."
}

variable "auto_minor_version_upgrade" {
  description = "Indicates that minor engine upgrades will be applied automatically to the DB instance during the maintenance window"
  type        = bool
  default     = false
}

variable "apply_immediately" {
  type        = bool
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window"
  default     = false
}

variable "deletion_protection" {
  type        = bool
  description = "If the DB instance should have deletion protection enabled"
  default     = false  // Changed true to false by DK
}

variable "monitoring_interval" {
  type        = number
  description = "Interval in seconds that metrics are collected, 0 to disable (values can only be 0, 1, 5, 10, 15, 30, 60)"
  default     = 0
}

variable "enable_backup_dr" {
  type        = bool
  default     = false
  description = "Enable replication of backup for DR"
}

variable "cpm_dr_encryption_key_source_region" {
  type        = string
  default     = "arn:aws:kms:eu-west-1:279898079618:key/c2290c18-11e6-4742-8b42-902c9ff63f08"
  description = "KMS key for DR on Backup account in same region than source"
}

variable "cpm_dr_encryption_key_target_region" {
  type        = string
  default     = "arn:aws:kms:eu-west-3:279898079618:key/1d9fdfb2-6298-42ff-8386-33e49a7ea39e"
  description = "KMS key for DR on Backup account in target region"
}

variable "backup_retention_period" {
  type        = string
  default     = "prod"
  description = "Backup retention. Valid values are 'prod', 'non-prod' or any number of days. BV standard is 31 for prod and 15 for non-prod."
}

variable "backup_window" {
  type        = string
  default     = "00:00-00:30"
  description = "Daily time range during which the backups happen. Default value is BV standard."
}

variable "enabled_enhanced_monitoring" {
  type        = bool
  description = "Set to false to prevent the module creating enhanced monitoring configuration and resources"
  default     = false
}

variable "enabled_cloudwatch_logs_exports" {
  type        = list(string)
  description = "List of log types to export to cloudwatch. If omitted, no logs will be exported. Valid values (depending on engine): alert, audit, error, general, listener, slowquery, trace, postgresql (PostgreSQL), upgrade (PostgreSQL)"
  default     = []
}

variable "performance_insights_enabled" {
  type        = bool
  default     = true
  description = "Whether to enable Performance Insights"
}

variable "performance_insights_retention_period" {
  type        = number
  default     = 7
  description = "The amount of time in days to retain Performance Insights data. Either 7 (7 days) or 731 (2 years). When specifying `performance_insights_retention_period`, `performance_insights_enabled` needs to be set to true"
}

variable "performance_insights_kms_key_id" {
  type        = string
  default     = ""
  description = "The ARN for the KMS key to encrypt Performance Insights data. When specifying `performance_insights_kms_key_id`, `performance_insights_enabled` needs to be set to true. Once KMS key is set, it can never be changed"
}

# ----------------------------------------------------------------------------------------------------------------------
# Scheduler
# ----------------------------------------------------------------------------------------------------------------------

variable "schedule_start" {
  type        = list(string)
  default     = []
  description = "Cron expression used to automatically start RDS instance. Possible to define multiple cron expression separate by '//' (double-slash)"
}

variable "schedule_stop" {
  type        = list(string)
  default     = []
  description = "Cron expression used to automatically start RDS instance. Possible to define multiple cron expression separate by '//' (double-slash)"
}

variable "enabled_cpm_backup" {
  type        = string
  default     = "Auto"
  description = "Allow to enable CPM backup. One of Auto, true or false. Auto will choose using environment: true for pr, and false for others."
}
