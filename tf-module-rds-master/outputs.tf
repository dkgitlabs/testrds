output "address" {
  description = "The address of the RDS instance"
  value       = aws_db_instance.rds.address
}

output "arn" {
  description = "The ARN of the RDS instance"
  value       = aws_db_instance.rds.arn
}

output "availability_zone" {
  description = "The availability zone of the RDS instance"
  value       = aws_db_instance.rds.availability_zone
}

output "id" {
  description = "The RDS instance ID"
  value       = aws_db_instance.rds.id
}

output "status" {
  description = "The RDS instance status"
  value       = aws_db_instance.rds.status
}

output "port" {
  description = "The database port"
  value       = aws_db_instance.rds.port
}

output "ssl_port" {
  description = "The database SSL port"
  value       = local.ssl_port
}

output "security_group_id" {
  value       = aws_security_group.rds.id
  description = "SG for additionnal rules added by client components: application server, batches"
}

output "password" {
  value       = random_password.rds_password
  description = "Auto generated password for the main DB user. IMPORTANT: this should be changed after provisioning"
}
