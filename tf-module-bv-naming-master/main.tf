locals {

  # Normalize inputs
  gap_code      = lower(var.gap_code)
  account       = lower(var.account)
  environment   = lower(var.environment)
  specification = lower(var.specification)
  number        = lower(var.number)

  # Build standard base name
  default_naming_order                     = ["account", "gap_code", "environment", "specification", "number"]
  default_naming_order_without_account     = ["gap_code", "environment", "specification", "number"]
  default_naming_order_without_environment = ["account", "gap_code", "specification", "number"]
  default_delimiter                        = "-"
  default_name_values_list = {
    account       = local.account
    gap_code      = local.gap_code
    environment   = local.environment
    specification = local.specification
    number        = local.number
  }
  default_name_parts_list = [for l in local.default_naming_order : local.default_name_values_list[l] if length(local.default_name_values_list[l]) > 0]
  default_name            = lower(join(local.default_delimiter, local.default_name_parts_list))

  default_name_without_account_parts_list     = [for l in local.default_naming_order_without_account : local.default_name_values_list[l] if length(local.default_name_values_list[l]) > 0]
  default_name_without_account                = lower(join(local.default_delimiter, local.default_name_without_account_parts_list))
  default_name_without_environment_parts_list = [for l in local.default_naming_order_without_environment : local.default_name_values_list[l] if length(local.default_name_values_list[l]) > 0]
  default_name_without_environment            = lower(join(local.default_delimiter, local.default_name_without_environment_parts_list))

  # -----------------------------------------------------------
  # BV standard tags
  # -----------------------------------------------------------
  billing_infos        = length(var.custom_billing) == 0 ? [{ gap_code = local.gap_code, environment = local.environment }] : var.custom_billing
  billing_info_gap-env = [for billing_info in local.billing_infos : join("-", [billing_info.gap_code, billing_info.environment])]
  bv_standard_tags = {
    "bv:gap-env-platform" = upper(join("+", local.billing_info_gap-env))
  }

  # -----------------------------------------------------------
  # BV standard naming
  # -----------------------------------------------------------
  bv_beanstalk_application_name     = "${upper(local.gap_code)} - ${var.specification}"
  bv_beanstalk_environment_name     = "ebs-${local.default_name}"
  bv_elasticache_name               = "eca-${local.default_name}"
  bv_s3_bucket_name                 = replace(local.default_name, "_", "")
  bv_security_group_name            = "sgr-${local.default_name_without_account}"
  bv_rds_name                       = "rds-${local.default_name}"
  bv_rds_parameter_group_name       = "rpg-${local.specification}-${length(local.environment) > 0 ? local.environment : "default"}-${local.gap_code}${length(local.number) > 0 ? "-${local.number}" : ""}"
  bv_rds_option_group_name          = "rog-${local.specification}-${length(local.environment) > 0 ? local.environment : "default"}-${local.gap_code}${length(local.number) > 0 ? "-${local.number}" : ""}"
  bv_waf_name                       = "waf-${local.default_name}"
  bv_kinesis_stream                 = "kin-${local.default_name}"
  bv_batch_compute_environment_name = "bec2-${local.default_name}"
  bv_batch_job_queue_name           = "bqe-${local.default_name_without_account}"
  bv_batch_job_definition           = "bdef-${local.default_name_without_account}"
  bv_sns_topic_name                 = "sns-${local.default_name_without_account}"
  bv_sfn_state_machine_name         = "stm-${local.default_name_without_account}"
  bv_ses_configuration_set_name     = "sec-${local.default_name}"
  bv_ses_event_destination_name     = "sed-${local.default_name}"
  bv_alb_name                       = "alb-${local.default_name}"
  bv_nlb_name                       = "nlb-${local.default_name}"
  bv_ec2_linux_instance_name        = "ecl-${local.default_name}"
  bv_ec2_windows_instance_name      = "ecw-${local.default_name}"
  bv_cloudwatch_sub_filter_name     = "csf-${local.default_name}"
  bv_ecs_cluster_name               = "ecsc-${local.default_name}"
  bv_ecs_service_name               = "ecss-${local.default_name}"
  bv_ecs_task_definition_name       = "ecst-${local.default_name}"
  bv_lambda_name                    = "lam-${local.default_name}"
  bv_sqs_queue_name                 = "sqs-${local.default_name_without_account}"
  bv_elasticsearch_name             = "elk-${local.default_name}"
  bv_fsx_name                       = "fsx-${local.default_name}"
  bv_api_gateway_name               = "${upper(local.gap_code)} - ${var.specification}"
  bv_vpc_link_name                  = "lnk-${local.default_name}"
  bv_glue_database_name             = local.default_name_without_environment
  bv_target_group_name              = "tgr-${local.default_name}"
  bv_efs_name                       = "efs-${local.default_name}"
  bv_redshift_name                  = "rsh-${local.default_name}"
}
