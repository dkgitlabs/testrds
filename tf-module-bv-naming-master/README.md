
# Module `tf-module-bv-naming`

Core Version Constraints:
* `~> 0.12.0`

## Input Variables
* `account` (required): Acount, e.g 'cis', 'cit', 'cdr' ...
* `custom_billing` (default `[]`): A list of ([gap_code],[environment]) for custom billing purpose, i.e. where billing is not the same than naming (mainly for shared resources).
* `environment` (default `""`): Two digits describing the environment type: e.g. 'dv', 'in', 'ut', 'pp', 'pr' or 'np', 'pr'
* `gap_code` (required): BV unique project code provided by GAP application (three digits)
* `number` (default `""`): Incremental number, e.g. '1, '2', '3' ...
* `specification` (default `""`): A specification (a role, a description, a situation)'

## Output Values
* `bv_alb_name`: BV Standard application load balancer name
* `bv_api_gateway_name`: BV Standard API Gateway name
* `bv_batch_compute_environment_name`: BV Standard Compute environment name
* `bv_batch_job_definition`: BV Standard Batch job definition name
* `bv_batch_job_queue_name`: BV Standard Batch job queue name
* `bv_beanstalk_application_name`: BV Standard beanstalk application name
* `bv_beanstalk_environment_name`: BV Standard beanstalk environment name
* `bv_cloudwatch_sub_filter_name`: BV Standard Cloudwatch Subscription Filter destination name
* `bv_ec2_linux_instance_name`: BV Standard EC2 name for a linux instance
* `bv_ec2_windows_instance_name`: BV Standard EC2 name for a Windows instance
* `bv_ecs_cluster_name`: BV Standard Cluster name
* `bv_ecs_service_name`: BV Standard Service name
* `bv_ecs_task_definition_name`: BV Standard Task Family Name
* `bv_efs_name`: BV Standard EFS name
* `bv_elasticache_name`: BV Standard ElastiCache cluster name
* `bv_elasticsearch_name`: BV Standard Elasticsearch name
* `bv_fsx_name`: BV Standard FSx name
* `bv_glue_database_name`: BV Standard Glue db name
* `bv_kinesis_stream`: BV Standard Kinesis stream name
* `bv_lambda_name`: BV Standard Lambda name
* `bv_nlb_name`: BV Standard network load balancer name
* `bv_rds_name`: BV Standard RDS name
* `bv_rds_option_group_name`: BV Standard RDS option groups name
* `bv_rds_parameter_group_name`: BV Standard RDS parameter groups name
* `bv_redshift_name`: BV Standard Redshift cluster name
* `bv_s3_bucket_name`: BV Standard S3 bucket name
* `bv_security_group_name`: BV Standard security group name
* `bv_ses_configuration_set_name`: BV Standard SES configuration set name
* `bv_ses_event_destination_name`: BV Standard SES event destination name
* `bv_sfn_state_machine_name`: BV Standard State machine name
* `bv_sns_topic_name`: BV Standard SNS topic name
* `bv_sqs_queue_name`: BV Standard SQS queue name
* `bv_standard_tags`: BV Standard Tag map
* `bv_target_group_name`: BV Standard Target group name
* `bv_vpc_link_name`: BV Standard VPC Link name
* `bv_waf_name`: BV Standard WAF name

