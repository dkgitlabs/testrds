output "bv_standard_tags" {
  value       = local.bv_standard_tags
  description = "BV Standard Tag map"
}

output "bv_beanstalk_application_name" {
  value       = local.bv_beanstalk_application_name
  description = "BV Standard beanstalk application name"
}

output "bv_beanstalk_environment_name" {
  value       = local.bv_beanstalk_environment_name
  description = "BV Standard beanstalk environment name"
}

output "bv_elasticache_name" {
  value       = local.bv_elasticache_name
  description = "BV Standard ElastiCache cluster name"
}

output "bv_s3_bucket_name" {
  value       = local.bv_s3_bucket_name
  description = "BV Standard S3 bucket name"
}

output "bv_security_group_name" {
  value       = local.bv_security_group_name
  description = "BV Standard security group name"
}

output "bv_rds_name" {
  value       = local.bv_rds_name
  description = "BV Standard RDS name"
}

output "bv_rds_parameter_group_name" {
  value       = local.bv_rds_parameter_group_name
  description = "BV Standard RDS parameter groups name"
}

output "bv_rds_option_group_name" {
  value       = local.bv_rds_option_group_name
  description = "BV Standard RDS option groups name"
}

output "bv_waf_name" {
  value       = local.bv_waf_name
  description = "BV Standard WAF name"
}

output "bv_kinesis_stream" {
  value       = local.bv_kinesis_stream
  description = "BV Standard Kinesis stream name"
}

output "bv_batch_compute_environment_name" {
  value       = local.bv_batch_compute_environment_name
  description = "BV Standard Compute environment name"
}

output "bv_batch_job_queue_name" {
  value       = local.bv_batch_job_queue_name
  description = "BV Standard Batch job queue name"
}

output "bv_batch_job_definition" {
  value       = local.bv_batch_job_definition
  description = "BV Standard Batch job definition name"
}

output "bv_sns_topic_name" {
  value       = local.bv_sns_topic_name
  description = "BV Standard SNS topic name"
}

output "bv_sfn_state_machine_name" {
  value       = local.bv_sfn_state_machine_name
  description = "BV Standard State machine name"
}

output "bv_ses_configuration_set_name" {
  value       = local.bv_ses_configuration_set_name
  description = "BV Standard SES configuration set name"
}

output "bv_ses_event_destination_name" {
  value       = local.bv_ses_event_destination_name
  description = "BV Standard SES event destination name"
}

output "bv_alb_name" {
  value       = local.bv_alb_name
  description = "BV Standard application load balancer name"
}

output "bv_nlb_name" {
  value       = local.bv_nlb_name
  description = "BV Standard network load balancer name"
}

output "bv_ec2_linux_instance_name" {
  value       = local.bv_ec2_linux_instance_name
  description = "BV Standard EC2 name for a linux instance"
}

output "bv_ec2_windows_instance_name" {
  value       = local.bv_ec2_windows_instance_name
  description = "BV Standard EC2 name for a Windows instance"
}

output "bv_cloudwatch_sub_filter_name" {
  value       = local.bv_cloudwatch_sub_filter_name
  description = "BV Standard Cloudwatch Subscription Filter destination name"
}

output "bv_ecs_cluster_name" {
  value       = local.bv_ecs_cluster_name
  description = "BV Standard Cluster name"
}

output "bv_ecs_task_definition_name" {
  value       = local.bv_ecs_task_definition_name
  description = "BV Standard Task Family Name"
}
output "bv_ecs_service_name" {
  value       = local.bv_ecs_service_name
  description = "BV Standard Service name"
}

output "bv_lambda_name" {
  value       = local.bv_lambda_name
  description = "BV Standard Lambda name"
}

output "bv_sqs_queue_name" {
  value       = local.bv_sqs_queue_name
  description = "BV Standard SQS queue name"
}

output "bv_elasticsearch_name" {
  value       = local.bv_elasticsearch_name
  description = "BV Standard Elasticsearch name"
}

output "bv_fsx_name" {
  value       = local.bv_fsx_name
  description = "BV Standard FSx name"
}

output "bv_glue_database_name" {
  value       = local.bv_glue_database_name
  description = "BV Standard Glue db name"
}

output "bv_api_gateway_name" {
  value       = local.bv_api_gateway_name
  description = "BV Standard API Gateway name"
}

output "bv_vpc_link_name" {
  value       = local.bv_vpc_link_name
  description = "BV Standard VPC Link name"
}

output "bv_target_group_name" {
  value       = local.bv_target_group_name
  description = "BV Standard Target group name"
}

output "bv_efs_name" {
  value       = local.bv_efs_name
  description = "BV Standard EFS name"
}

output "bv_redshift_name" {
  value       = local.bv_redshift_name
  description = "BV Standard Redshift cluster name"
}
