variable "account" {
  type        = string
  description = "Acount, e.g 'cis', 'cit', 'cdr' ..."
}

variable "gap_code" {
  type        = string
  description = "BV unique project code provided by GAP application (three digits)"
}

variable "environment" {
  type        = string
  default     = ""
  description = "Two digits describing the environment type: e.g. 'dv', 'in', 'ut', 'pp', 'pr' or 'np', 'pr'"
}

variable "specification" {
  type        = string
  default     = ""
  description = "A specification (a role, a description, a situation)'"
}

variable "number" {
  type        = string
  default     = ""
  description = "Incremental number, e.g. '1, '2', '3' ..."
}

variable "custom_billing" {
  type = list(object({
    gap_code    = string
    environment = string
  }))
  default     = []
  description = "A list of ([gap_code],[environment]) for custom billing purpose, i.e. where billing is not the same than naming (mainly for shared resources)."
}
