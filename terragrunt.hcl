terraform {
  #source = "git::https://git.bureauveritas.com/aws-cloud/data/tf-module-rds.git//?ref=release-1.3.0"
  #source = "git::https://gitlab.com/dkgitlabs/testrds/-/tree/master/tf-module-rds-master"
  source = "../../..//tf-module-rds-master"
}
/* //Commented by DK ----tested gitlab update
dependency "vpc" {
  config_path = "../../../../_shared/00_Network/001_VPC"
}

// dcp-ima-pr-dbmigration
dependency "s3_dbmigration" {
  config_path = "../../../../_shared/20_Data/222_S3_DbMig"
}


include {
  path = find_in_parent_folders()
}
*/ //Commented by DK
inputs = {
  gap_code                = "testdk0"
  environment             = "pp"
  number                  = "1"

  engine                  = "sqlserver-web"
  engine_version          = "15.00.4043.16.v1"
  #engine_version          = "14.00.3356.20.v1"
  instance_type           = "db.t3.small"
  license_model           = "license-included"

/*
 # MYSQL Engine
  engine                  = "mysql"
  engine_version          = "5.7.30"
  instance_type           = "db.m5.xlarge"

# Oracle Engine
  engine                  = "oracle-ee"
  engine_version          = "12.2.0.1.ru-2020-10.rur-2020-10.r1"
  instance_type           = "db.m5.xlarge"
  license_model           = "bring-your-own-license"
*/


  #subnet_ids              = dependency.vpc.outputs.db_subnets_ids //Commented by DK
  #vpc_id                  = dependency.vpc.outputs.id  //Commented by DK
  backup_retention_period = "non-prod"
  enable_backup_dr        = true
  #map-migrated            = "d-server-01z4kbrsppom3v" // Hostname	dg0-pr-database / Server ID	d-server-01z4kbrsppom3v

  allocated_storage       = 30
  #max_allocated_storage   = 1500

  maintenance_window      = "sat:21:45-sat:22:15"

  enabled_enhanced_monitoring = true
  monitoring_interval = 10
  performance_insights_enabled = true

  allowed_cidr_blocks     = [ "10.64.0.0/16"        # Allow access from NDC
                              ,"10.136.32.0/22"     # Allow access from GSSC
                              ,"10.0.1.94/32"       # Allow access from MY EC2
                            ]

}
